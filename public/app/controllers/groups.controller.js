checkInApp.controller('groupsController', ['$location', '$scope', 'authService', 'entityService', 'uiGridConstants',
    function($location, $scope, authService, entityService, uiGridConstants) {

        // State variables
        $scope.showAddGroup = false;
        $scope.showEditGroup = false;
        $scope.showInviteToGroup = false;
        $scope.selectedGroupId = null;

        // Grid Options
        $scope.gridOptions = {
            columnDefs: [{
                    field: 'name',
                    sort: {
                        direction: uiGridConstants.ASC,
                        priority: 1
                    }
                },
                { field: 'owner' },
                { field: 'members' },
                { field: 'invited' }
            ],
            data: [],
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            enableRowSelection: true,
            multiSelect: false,
            noUnselect: false,
            onRegisterApi: function(gridApi) {
                // Sets the gridApi variable
                $scope.gridApi = gridApi;
                // Listens for Row Selection Events
                $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    if (row && row.isSelected && row.entity && row.entity._id) {
                        if ($scope.showInviteToGroup) {
                            showForm('invite');
                            $scope.selectedGroupId = row.entity._id;
                        } else {
                            showForm('edit');
                            $scope.selectedGroupId = row.entity._id;
                            $scope.groupName = row.entity.name;
                        }
                    } else {
                        showForm('none');
                        $scope.selectedGroupId = null;
                        $scope.groupName = null;
                    }
                });
            }
        };

        $scope.addGroup = function() {
            // Create the body of the request
            var data = {
                name: $scope.groupName
            };

            // Send the request to create the group
            entityService.createGroup(data)
                .then(function(result) {
                    showForm('none');
                    $scope.groupName = null;
                    $scope.errorMessage = null;
                    $scope.getUserGroups();

                })
                .catch(function(error) {
                    $scope.errorMessage = error;
                });
        }

        $scope.deleteGroup = function() {
            if (!$scope.selectedGroupId) {
                return; // To-Do
            }

            entityService.deleteGroup($scope.selectedGroupId)
                .then(function() {
                    showForm('none');
                    $scope.groupName = null;
                    $scope.selectedGroupId = null;
                    $scope.errorMessage = null;
                    $scope.gridApi.selection.clearSelectedRows();
                    $scope.getUserGroups();
                })
                .catch(function(error) {
                    $scope.errorMessage = error;
                });
        }

        $scope.editGroup = function() {
            // Create the body of the request
            var data = {
                groupId: $scope.selectedGroupId,
                name: $scope.groupName
            };

            // Send the request to create the group
            entityService.updateGroup(data)
                .then(function(result) {
                    showForm('none');
                    $scope.groupName = null;
                    $scope.selectedGroupId = null;
                    $scope.errorMessage = null;
                    $scope.gridApi.selection.clearSelectedRows();
                    $scope.getUserGroups();
                })
                .catch(function(error) {
                    $scope.errorMessage = error;
                });
        }

        $scope.getUserGroups = function() {
            // Only run if the current user is logged in
            if (!authService.isLoggedIn()) {
                return;
            }

            // Get the current user
            entityService.getCurrentUser()
                .then(function(user) {

                    if (user) {
                        entityService.getUserGroups(user._id)
                            .then(function(groups) {
                                $scope.gridOptions.data = groups;
                            })
                            .catch(function(error) {
                                $scope.errorMessage = 'Unable to retrieve groups.';
                            });
                    } else {
                        $scope.errorMessage = 'Unable to retrieve groups.';
                    }

                })
                .catch(function(error) {
                    $scope.errorMessage = 'Unable to retrieve groups.';
                });
        }

        $scope.inviteToGroup = function() {

            // Validate the required fields
            if (!$scope.selectedGroupId) {
                $scope.errorMessage = "Please select a group to invite them to."
                return;
            }
            if (!$scope.inviteEmail) {
                $scope.errorMessage = "Please enter the invitee's email address."
                return;
            }

            // Send the request to invite to group
            entityService.inviteToGroup($scope.selectedGroupId, $scope.inviteEmail)
                .then(function(result) {
                    showForm('none');
                    $scope.groupName = null;
                    $scope.selectedGroupId = null;
                    $scope.errorMessage = null;
                    $scope.gridApi.selection.clearSelectedRows();
                    $scope.getUserGroups();
                })
                .catch(function(error) {
                    $scope.errorMessage = error;
                });
        }

        function showForm(form) {
            switch (form) {
                case 'add':
                    $scope.showAddGroup = true;
                    $scope.showEditGroup = false;
                    $scope.showInviteToGroup = false;
                    break;
                case 'edit':
                    $scope.showAddGroup = false;
                    $scope.showEditGroup = true;
                    $scope.showInviteToGroup = false;
                    break;
                case 'invite':
                    $scope.showAddGroup = false;
                    $scope.showEditGroup = false;
                    $scope.showInviteToGroup = true;
                    break;
                case 'none':
                    $scope.showAddGroup = false;
                    $scope.showEditGroup = false;
                    $scope.showInviteToGroup = false;
                    break;
            }
        }

        $scope.switchAddGroup = function() {
            if ($scope.showAddGroup) {
                showForm('none');
            } else {
                showForm('add');
                $scope.groupName = null;
                $scope.selectedGroupId = null;
                $scope.gridApi.selection.clearSelectedRows();
            }
        }

        $scope.switchInviteToGroup = function() {
            if ($scope.showInviteToGroup && $scope.showInviteToGroup == true) {
                showForm('none');
            } else {
                showForm('invite');
                $scope.inviteEmail = null;
            }
        }
    }
]); // controller()