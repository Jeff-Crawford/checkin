checkInApp.controller('dashboardController', ['$location', '$scope', 'authService', 'entityService',
    function($location, $scope, authService, entityService) {

        $scope.groupCount = 0;
        $scope.eventCount = 0;
        $scope.showProfileAlert = false;

        // Check the profile to see if required data has been provided
        $scope.checkProfile = function() {
            // Only run if the current user is logged in
            if (!authService.isLoggedIn()) {
                return;
            }

            // Get the current user
            entityService.getCurrentUser()
                .then(function(user) {
                    if (user && (!user.name || !user.email)) {
                        $scope.showProfileAlert = true;
                    }
                    getGroupCount(user);
                    getEventCount(user);
                }) // entityService.getCurrentUser()

        };

        // Get the group count
        function getGroupCount(user) {
            if (user) {
                entityService.getUserGroups(user._id)
                    .then(function(groups) {
                        $scope.groupCount = groups.length;
                    });
            }
        }

        // Get the event count
        function getEventCount(user) {
            if (user) {
                entityService.getUserEvents(user._id)
                    .then(function(events) {
                        $scope.eventCount = events.length;
                    });
            }
        }

    }
]); // controller()