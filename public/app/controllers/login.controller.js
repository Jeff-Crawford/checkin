checkInApp.controller('loginController', ['$location', '$scope', 'authService',
    function($location, $scope, authService) {

        // Submit the login info
        $scope.login = function() {
            $scope.loginError = null;
            authService.login($scope.loginForm.email, $scope.loginForm.password)
                .then(function() {
                    $scope.loginForm = {};
                    $location.path('/dashboard');
                })
                .catch(function() {
                    $scope.loginForm.password = null;
                    $scope.loginError = 'Login failed.';
                }); // authService.login()
        }; // login()

        $scope.resetPassword = function() {
            console.log('Called submitResetPassword()');
            $scope.requestedReset = true;
        }; // resetPassword()

    }
]); // controller()