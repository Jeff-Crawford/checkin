checkInApp.controller('navbarController', ['$location', '$scope', 'authService', 'entityService',
    function($location, $scope, authService, entityService) {

        // Model - Defaults
        $scope.brandRoute = '#!/';
        $scope.currentUser = null;
        $scope.hideAppMenus = true;
        $scope.hideLoginMenus = false;

        // Onload
        (function() {
            displayNavItems($location.path());
            getProfile();
        })();

        // Watch for route changes
        $scope.$on('$routeChangeSuccess', function(event, current, previous) {
            displayNavItems(current.$$route.originalPath);
            getProfile();
        }); // $on()

        // Logout
        $scope.logout = function() {
            authService.logout()
                .then(function() {
                    $scope.brandRoute = '#!/';
                    $scope.currentUser = null;
                    $location.path('/');
                })
                .catch(function() {
                    $scope.brandRoute = '#!/';
                    $scope.currentUser = null;
                    $location.path('/');
                });
        }; // logout()

        // Set which navbar items are displayed
        function displayNavItems(rotue) {
            var loggedIn = authService.isLoggedIn();
            if (loggedIn) {
                $scope.hideLoginMenus = true;

                rotue = (rotue) ? rotue.toLowerCase() : '/';
                if ((rotue.substring(0, 10) == '/dashboard' ||
                        rotue.substring(0, 7) == '/events' ||
                        rotue.substring(0, 7) == '/groups' ||
                        rotue.substring(0, 8) == '/profile')) {
                    $scope.brandRoute = '#!/dashboard';
                    $scope.hideAppMenus = false;
                } else {
                    $scope.brandRoute = '#!/';
                    $scope.hideAppMenus = true;
                }

            } else {
                $scope.hideAppMenus = true;
                $scope.hideLoginMenus = false;
            }

        } // displayNavItems()

        // Get the current user's profile
        function getProfile() {
            // Only run if the current user is logged in
            if (!authService.isLoggedIn()) {
                return
            }

            // Get the current user
            entityService.getCurrentUser()
                .then(function(user) {
                    if (user) {
                        $scope.userName = user.name;
                    }
                }) // entityService.getCurrentUser()
        };

    }
]); // controller()