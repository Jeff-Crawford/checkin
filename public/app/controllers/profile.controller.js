checkInApp.controller('profileController', ['$location', '$scope', 'authService', 'entityService',
    function($location, $scope, authService, entityService) {

        // State variables
        $scope.currentUser;

        // Loads the profile info
        $scope.getProfile = function() {
            // Only get the current user if they are logged in
            if (authService.isLoggedIn()) {
                entityService.getCurrentUser()
                    .then(function(user) {
                        if (user) {
                            $scope.currentUser = user;
                            $scope.profileName = user.name;
                            $scope.profileEmail = user.email;
                            $scope.profileMobile = user.mobile;
                        } else {
                            $scope.errorMessage = 'Unable to retrieve profile.';
                        }
                    })
                    .catch(function() {
                        $scope.errorMessage = 'Unable to retrieve profile.';
                    }) // entityService.getUsers()

            }
        }

        // Updates the profile info
        $scope.updateProfile = function() {
            var data = {
                email: $scope.profileEmail,
                mobile: $scope.profileMobile,
                name: $scope.profileName,
                password: $scope.profilePassword,
                userId: $scope.currentUser._id
            };
            entityService.updateUser(data)
                .then(function(response) {
                    $scope.updateMessage = response;
                })
                .catch(function(error) {
                    $scope.errorMessage = error;
                }) // entityService.updateProfile()
        }

    }
]); // controller()