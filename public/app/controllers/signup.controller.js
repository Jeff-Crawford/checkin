checkInApp.controller('signupController', ['$http', '$scope', 'authService',
    function($http, $scope, authService) {

        $scope.hasSubmitted = false;

        // submit sign up info
        $scope.signup = function() {
            $scope.signupError = null;
            authService.registration($scope.signupForm.email, $scope.signupForm.password)
                .then(function(response) {
                    $scope.signupForm = {};
                    $scope.hasSubmitted = true;
                })
                .catch(function(error) {
                    $scope.signupForm = {};
                    $scope.hasSubmitted = false;
                    $scope.signupError = error;
                }); // authService.registration()
        }; // signup()

    }
]); // checkInApp.controller('signupController')