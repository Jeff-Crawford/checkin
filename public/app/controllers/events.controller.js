checkInApp.controller('eventsController', ['$location', '$scope', 'authService', 'entityService', 'uiGridConstants',
    function($location, $scope, authService, entityService, uiGridConstants) {

        // State variables
        $scope.showAddEvent = false;
        $scope.showEditEvent = false;
        $scope.showCopyEvent = false;
        $scope.selectedEventId = null;

        // Grid Options
        $scope.gridOptions = {
            columnDefs: [
                { name: 'Group', field: 'group.name' },
                {
                    name: 'Event',
                    field: 'name',
                    sort: {
                        direction: uiGridConstants.ASC,
                        priority: 1
                    }
                },
                { field: 'enabled' },
                { name: 'Owner', field: 'owner.name' }
            ],
            data: [],
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            enableRowSelection: true,
            multiSelect: false,
            noUnselect: false,
            onRegisterApi: function(gridApi) {
                // Sets the gridApi variable
                $scope.gridApi = gridApi;
                // Listens for Row Selection Events
                $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    if (row && row.isSelected && row.entity && row.entity._id) {
                        if ($scope.showCopyEvent) {
                            showForm('copy');
                            $scope.selectedEventId = row.entity._id;
                            $scope.eventName = row.entity.name;
                        } else {
                            showForm('edit');
                            $scope.selectedEventId = row.entity._id;
                            // $scope.eventGroup = row.entity.group;
                            $scope.eventEnabled = row.entity.enabled;
                            $scope.eventName = row.entity.name;
                            // $scope.eventNotified = row.entity.whoIsNotified;
                            // $scope.eventChecksIn = row.entity.whoChecksIn;
                            $scope.eventNotes = row.entity.notes;
                            $scope.eventfrequency = row.entity.frequency;
                            $scope.eventStartDate = row.entity.startDate;
                            $scope.eventRecurDays = row.entity.recurDays;
                            $scope.eventRecurWeeks = row.entity.recurWeeks;
                            // $scope.eventRecurWeekOnSunday = null;
                            // $scope.eventRecurWeekOnMonday = null;
                            // $scope.eventRecurWeekOnTuesday = null;
                            // $scope.eventRecurWeekOnWednesday = null;
                            // $scope.eventRecurWeekOnThursday = null;
                            // $scope.eventRecurWeekOnFriday = null;
                            // $scope.eventRecurWeekOnSaturday = null;
                            //    recurWeekOnDays: [{ type: String }],
                        }
                    } else {
                        showForm('none');
                        $scope.selectedEventId = null;
                        $scope.eventName = null;
                    }
                });
            }
        };

        $scope.addEvent = function() {
            // Create the body of the request
            var data = {
                enabled: $scope.eventEnabled,
                name: $scope.eventName,
                notes: $scope.eventNotes,
                frequency: $scope.eventfrequency,
                startDate: $scope.eventStartDate,
                recurDays: $scope.eventRecurDays,
                recurWeeks: $scope.eventRecurWeeks
            };

            // Send the request to create the event
            entityService.createEvent(data)
                .then(function(result) {
                    showForm('none');
                    clearDataEntry();
                    clearSelelction();
                    $scope.getUserEvents();
                })
                .catch(function(error) {
                    $scope.errorMessage = error;
                });
        }

        function clearDataEntry() {
            $scope.eventGroup = null;
            $scope.eventEnabled = null;
            $scope.eventName = null;
            $scope.eventNotified = null;
            $scope.eventChecksIn = null;
            $scope.eventChecksIn = null;
            $scope.eventNotes = null;
            $scope.eventfrequency = null;
            $scope.eventStartDate = null;
            $scope.eventRecurDays = null;
            $scope.eventRecurWeeks = null;
            $scope.eventRecurWeekOnSunday = null;
            $scope.eventRecurWeekOnMonday = null;
            $scope.eventRecurWeekOnTuesday = null;
            $scope.eventRecurWeekOnWednesday = null;
            $scope.eventRecurWeekOnThursday = null;
            $scope.eventRecurWeekOnFriday = null;
            $scope.eventRecurWeekOnSaturday = null;
            $scope.updateMessage = null;
            $scope.errorMessage = null;
        }

        function clearSelelction() {
            $scope.selectedEventId = null;
            $scope.gridApi.selection.clearSelectedRows();
        }

        $scope.copyEvent = function() {
            $scope.errorMessage = 'This feature doesn\'s work yet.';
        }

        $scope.deleteEvent = function() {
            if (!$scope.selectedEventId) {
                return; // To-Do
            }

            entityService.deleteEvent($scope.selectedEventId)
                .then(function() {
                    showForm('none');
                    clearDataEntry();
                    clearSelelction();
                    $scope.getUserEvents();
                })
                .catch(function(error) {
                    $scope.errorMessage = error;
                });
        }

        $scope.editEvent = function() {
            // Create the body of the request
            var data = {
                eventId: $scope.selectedEventId,
                enabled: $scope.eventEnabled,
                name: $scope.eventName,
                notes: $scope.eventNotes,
                frequency: $scope.eventfrequency,
                startDate: $scope.eventStartDate,
                recurDays: $scope.eventRecurDays,
                recurWeeks: $scope.eventRecurWeeks
            };

            // Send the request to create the event
            entityService.updateEvent(data)
                .then(function(result) {
                    showForm('none');
                    clearDataEntry();
                    clearSelelction();
                    $scope.getUserEvents();
                })
                .catch(function(error) {
                    $scope.errorMessage = error;
                });
        }

        $scope.getUserEvents = function() {
            // Only run if the current user is logged in
            if (!authService.isLoggedIn()) {
                return;
            }

            // Get the current user
            entityService.getCurrentUser()
                .then(function(user) {

                    if (user) {
                        entityService.getUserEvents(user._id)
                            .then(function(events) {
                                $scope.gridOptions.data = events;
                            })
                            .catch(function(error) {
                                $scope.errorMessage = 'Unable to retrieve events.';
                            });
                    } else {
                        $scope.errorMessage = 'Unable to retrieve events.';
                    }

                })
                .catch(function(error) {
                    $scope.errorMessage = 'Unable to retrieve events.';
                });
        }

        function showForm(form) {
            switch (form) {
                case 'add':
                    $scope.showAddEvent = true;
                    $scope.showEditEvent = false;
                    $scope.showCopyEvent = false;
                    break;
                case 'edit':
                    $scope.showAddEvent = false;
                    $scope.showEditEvent = true;
                    $scope.showCopyEvent = false;
                    break;
                case 'copy':
                    $scope.showAddEvent = false;
                    $scope.showEditEvent = false;
                    $scope.showCopyEvent = true;
                    break;
                case 'none':
                    $scope.showAddEvent = false;
                    $scope.showEditEvent = false;
                    $scope.showCopyEvent = false;
                    break;
            }
        }

        $scope.switchAddEvent = function() {
            if ($scope.showAddEvent) {
                showForm('none');
            } else {
                showForm('add');
                clearDataEntry();
                clearSelelction();
            }
        }

        $scope.switchCopyEvent = function() {
            if ($scope.showCopyEvent) {
                showForm('none');
            } else {
                showForm('copy');
            }
        }

    }
]); // controller()