// Frontend Routes
checkInApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.caseInsensitiveMatch = true;
        $routeProvider
            .when('/', {
                controller: '',
                templateUrl: './app/views/landing.view.html',
                access: { restricted: false }
            })
            .when('/dashboard', {
                controller: 'dashboardController',
                templateUrl: './app/views/dashboard.view.html',
                access: { restricted: true }
            })
            .when('/events', {
                controller: 'eventsController',
                templateUrl: './app/views/events.view.html',
                access: { restricted: true }
            })
            .when('/groups', {
                controller: 'groupsController',
                templateUrl: './app/views/groups.view.html',
                access: { restricted: true }
            })
            .when('/login', {
                controller: 'loginController',
                templateUrl: './app/views/login.view.html',
                access: { restricted: false }
            })
            .when('/profile', {
                controller: 'profileController',
                templateUrl: './app/views/profile.view.html',
                access: { restricted: true }
            })
            .when('/signup', {
                controller: 'signupController',
                templateUrl: './app/views/signup.view.html',
                access: { restricted: false }
            })
            .otherwise({
                redirectTo: '/',
            });
    }
]); // config()

// Middleware - On protected page, redirect to login, if not logged in
checkInApp.run(['$location', '$rootScope', 'authService', function($location, $rootScope, authService) {

    // Add a listener for route changes
    $rootScope.$on('$routeChangeStart', function(event, next, current) {

        // Check if the next route is restricted and if client side says the current user is not logged in
        if (next.$$route.access && next.$$route.access.restricted && !authService.isLoggedIn()) {

            // Get the status of the user from the server
            authService.getLoginStatus()
                .then(function() {

                    // If the user is still not logged, then kick um out
                    if (!authService.isLoggedIn()) {
                        $location.path('/login');
                    }
                });
        }

    }); // on()

}]); // run ()