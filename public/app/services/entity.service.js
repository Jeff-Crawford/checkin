checkInApp.factory('entityService', ['$http', '$q', function($http, $q) {

    // Return an object with the methods that can be used
    return ({
        clearState: clearState,
        createEvent: createEvent,
        createGroup: createGroup,
        deleteEvent: deleteEvent,
        deleteGroup: deleteGroup,
        getCurrentUser: getCurrentUser,
        getUsers: getUsers,
        getUserEvents: getUserEvents,
        getUserGroups: getUserGroups,
        inviteToGroup: inviteToGroup,
        updateEvent: updateEvent,
        updateGroup: updateGroup,
        updateUser: updateUser
    });

    // State variables
    var currentUser;
    var events;
    var groups;
    var users;

    function clearState() {
        currentUser = null;
        events = null;
        groups = null;
        users = null;
    }

    function createEvent(data) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        $http.post('/api/events/', data)
            .then(function(response) {
                deferred.resolve('Inserted.');
            })
            .catch(function(error) {
                deferred.reject('Insert Failed.');
            }); // $http.post()

        return deferred.promise;
    } // createEvent()

    function createGroup(data) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        $http.post('/api/groups/', data)
            .then(function(response) {
                deferred.resolve('Inserted.');
            })
            .catch(function(error) {
                deferred.reject('Insert Failed.');
            }); // $http.post()

        return deferred.promise;
    } // createGroup()

    function deleteEvent(eventId) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        $http.delete('/api/events/' + eventId)
            .then(function(response) {
                deferred.resolve('Deleted.');
            })
            .catch(function(error) {
                deferred.reject('Delete Failed.');
            }); // $http.delete()

        return deferred.promise;
    } // deleteEvent()

    function deleteGroup(groupId) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        $http.delete('/api/groups/' + groupId)
            .then(function(response) {
                deferred.resolve('Deleted.');
            })
            .catch(function(error) {
                deferred.reject('Delete Failed.');
            }); // $http.delete()

        return deferred.promise;
    } // deleteGroup()

    function getCurrentUser() {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        if (currentUser) {
            // Return the current user from state
            deferred.resolve(currentUser);
        } else {
            // Return the current user from the server
            getUsers(['current'])
                .then(function(users) {
                    if (users.length > 0) {
                        currentUser = users[0];
                        deferred.resolve(currentUser);
                    } else {
                        deferred.reject('Requesting Current User Failed.');
                    }
                })
                .catch(function(error) {
                    deferred.reject('Requesting Current User Failed.');
                }); // entityService.getUsers()
        }

        return deferred.promise;
    } // getCurrentUser()

    function getUsers(userIds) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();
        var promises = [];
        var result = [];

        // Initialize the users list
        if (!users) {
            users = [];
        }

        // For each requested userId
        for (var i = 0, len = userIds.length; i < len; i++) {

            // Check if the user has already been requested
            var index = arrayObjectIndexOf(users, '_id', userIds[i]);
            if (index > -1) {
                result.push(users[index]);
            } else {
                // Promise to get the user
                var promise = $http.get('/api/users/' + userIds[i]);
                promises.push(promise);
            }

        }

        // Resolve all promises
        $q.all(promises)
            .then(function(response) {
                for (var i = 0, len = response.length; i < len; i++) {
                    result.push(response[i].data);
                }
                deferred.resolve(result);
            }); // $q.all

        return deferred.promise;
    } // getUsers()

    function getUserEvents(userId) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        // Ensure a userId was provided
        if (!userId) {
            deferred.reject('Missing User Id.');
        }

        // Request the events for the requested user
        $http.get('/api/users/' + userId + '/events')
            .then(function(response) {

                // Clear the event list
                events = [];

                // Populate the event list from the response
                for (var i = 0, len = response.data.length; i < len; i++) {
                    events.push(response.data[i]);
                }

                // Return the results
                deferred.resolve(events);

            })
            .catch(function(error) {
                deferred.reject('Requesting Events Failed.');
            }); // $http.get()

        return deferred.promise;
    } // getUserEvents()

    function getUserGroups(userId) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        // Ensure a userId was provided
        if (!userId) {
            deferred.reject('Missing User Id.');
        }

        // Request the groups for the requested user
        $http.get('/api/users/' + userId + '/groups')
            .then(function(response) {

                // Clear the event list
                groups = [];

                // Populate the event list from the response
                for (var i = 0, len = response.data.length; i < len; i++) {
                    groups.push(response.data[i]);
                }

                // Return the results
                deferred.resolve(groups);

            })
            .catch(function(error) {
                deferred.reject('Requesting Groups Failed.');
            }); // $http.get()

        return deferred.promise;
    } // getUserGroups()

    function inviteToGroup(groupId, email) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        // Ensure a groupId was provided
        if (groupId) {
            groupId = groupId.trim();
        } else {
            deferred.reject('Missing Group Id.');
        }

        // Ensure an email was provided
        if (email) {
            email = email.toLowerCase().trim();
        } else {
            deferred.reject('Missing Email.');
        }

        // Send the request to invite the person to the group
        $http.put('/api/groups/' + groupId + '/invite/' + email)
            .then(function(response) {
                deferred.resolve('Invited.');
            })
            .catch(function(error) {
                deferred.reject('Invitation Failed.');
            }); // $http.put()

        return deferred.promise;
    } // inviteToGroup ()

    function updateEvent(data) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        // Ensure a eventId was provided
        if (!data.eventId) {
            deferred.reject('Missing Event Id.');
        }

        // Update the event with the provided data
        $http.put('/api/events/' + data.eventId, data)
            .then(function(response) {
                deferred.resolve('Updated.');
            })
            .catch(function(error) {
                deferred.reject('Update Failed.');
            }); // $http.put()

        return deferred.promise;
    } // updateEvent()

    function updateGroup(data) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        // Ensure a groupId was provided
        if (!data.groupId) {
            deferred.reject('Missing Group Id.');
        }

        // Update the group with the provided data
        $http.put('/api/groups/' + data.groupId, data)
            .then(function(response) {
                deferred.resolve('Updated.');
            })
            .catch(function(error) {
                deferred.reject('Update Failed.');
            }); // $http.put()

        return deferred.promise;
    } // updateGroup()

    function updateUser(data) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        // Ensure a userId was provided
        if (!data.userId) {
            deferred.reject('Missing User Id.');
        }

        // Update the user with the provided data
        $http.put('/api/users/' + data.userId, data)
            .then(function(response) {
                currentUser = null;
                deferred.resolve('Updated.');
            })
            .catch(function(error) {
                deferred.reject('Update Failed.');
            }); // $http.put()

        return deferred.promise;
    } // updateUser()

}]); // factory()