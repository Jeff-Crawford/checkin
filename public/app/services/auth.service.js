checkInApp.factory('authService', ['$http', '$q', 'entityService', function($http, $q, entityService) {

    // Return an object with the methods that can be used
    return ({
        getLoginStatus: getLoginStatus,
        isLoggedIn: isLoggedIn,
        login: login,
        logout: logout,
        registration: registration,
    });

    // State variables
    var loggedIn = false;

    // Check the server if authenticated
    function getLoginStatus() {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        $http.get('/api/authenticated')
            .then(function(response) {
                if (response.status == 200) {
                    loggedIn = true;
                    deferred.resolve('Logged In');
                } else {
                    loggedIn = false;
                    deferred.resolve('Not Logged In');
                }
            }, function(error) {
                loggedIn = false;
                deferred.resolve('Not Logged In');
            }); //$http.get

        return deferred.promise;
    } // getLoginStatus()

    function isLoggedIn() {
        if (loggedIn && loggedIn == true) {
            return true;
        } else {
            return false;
        }
    } // isLoggedIn()

    function login(email, password) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        $http.post('/api/login', { "email": email, "password": password })
            .then(function(response) {
                if (response.status == 200) {
                    loggedIn = true;
                    deferred.resolve('Login Successful');
                } else {
                    loggedIn = false;
                    deferred.reject('Login Failed');
                }
            }, function(error) {
                loggedIn = false;
                deferred.reject('Login Failed');
            }); //$http.post

        return deferred.promise;
    } // login()

    function logout() {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        $http.post('/api/logout')
            .then(function(response) {
                loggedIn = false;
                entityService.clearState();
                deferred.resolve('Logged Out');
            }, function(error) {
                loggedIn = false;
                entityService.clearState();
                deferred.reject('Logged Out');
            }); // $http.post()

        return deferred.promise;
    } // logout()

    function registration(email, password) {
        // Use the older defer promises for greater client support
        var deferred = $q.defer();

        // Default
        registered = false;

        if (!email || !password) {
            deferred.reject('Missing email or password');
        }

        $http.post('/api/registration', { "email": email, "password": password })
            .then(function(response) {
                if (response && response.status == 201) {
                    registered = true;
                    deferred.resolve();
                } else {
                    deferred.reject();
                }
            }, function(error) {
                if (error.status == 409) {
                    deferred.reject('That email address is already associated with a member.  Please log in.');
                } else {
                    deferred.reject('Internal Server Error');
                }
            }); //$http.post

        return deferred.promise;
    } // registration()

}]); // factory()