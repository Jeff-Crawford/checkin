checkInApp.directive('myNavbar', function () {
    return {
        controller: 'navbarController',
        'restrict': 'A',
        'templateUrl': './app/views/navbar.view.html'
    };
});