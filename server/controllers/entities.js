// Load dependant modules
var mongoose = require('mongoose');
var path = require('path');
var objHelper = require('../utilities/objectHelper.js')

// Initialize modules
var log = require('../config/logger.js');
var mailgun = require('../config/mailgun.js');

module.exports = {

    /******************************************************
     * Function: createEvent
     * Description: Creates the requested event.
     ******************************************************/
    createEvent: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var enabled, frequency, group, name, notes, recurDays, recurMonths, recurMonthOnDays;
            var recurWeeks, recurWeekOnDays, startDate, whoChecksIn, whoIsNotified;
            if (request.body.enabled) enabled = (request.body.enabled === true);
            if (request.body.frequency) frequency = request.body.frequency.trim();
            if (request.body.group) group = request.body.group.trim();
            if (request.body.name) name = request.body.name.trim();
            if (request.body.notes) notes = request.body.notes.trim();
            if (request.body.recurDays) recurDays = request.body.recurDays;
            if (request.body.recurMonths) recurMonths = objHelper.arrayTrimStrings(request.body.recurMonths);
            if (request.body.recurMonthOnDays) recurMonthOnDays = objHelper.arrayTrimStrings(request.body.recurMonthOnDays);
            if (request.body.recurWeeks) recurWeeks = request.body.recurWeeks;
            if (request.body.recurWeekOnDays) recurWeekOnDays = objHelper.arrayTrimStrings(request.body.recurWeekOnDays);
            if (request.body.startDate) startDate = request.body.startDate;
            if (request.body.whoChecksIn) whoChecksIn = objHelper.arrayTrimStrings(request.body.whoChecksIn);
            if (request.body.whoIsNotified) whoIsNotified = objHelper.arrayTrimStrings(request.body.whoIsNotified);

            // Create a new event
            var Event = mongoose.model('Event');
            var newEvent = new Event();
            newEvent.enabled = enabled;
            if (frequency) { newEvent.frequency = frequency; }
            if (group) { newEvent.group = group; }
            if (name) { newEvent.name = name; }
            if (notes) { newEvent.notes = notes; }
            newEvent.owner = uid;
            if (recurDays) { newEvent.recurDays = recurDays; }
            if (recurMonths) { newEvent.recurMonths = recurMonths; }
            if (recurMonthOnDays) { newEvent.recurMonthOnDays = recurMonthOnDays; }
            if (recurWeeks) { newEvent.recurWeeks = recurWeeks; }
            if (recurWeekOnDays) { newEvent.recurWeekOnDays = recurWeekOnDays; }
            if (startDate) { newEvent.startDate = startDate; }
            if (whoChecksIn) { newEvent.whoChecksIn = whoChecksIn; }
            if (whoIsNotified) { newEvent.whoIsNotified = whoIsNotified; }

            // Insert the new event
            Event.create(newEvent)
                .then((event) => {
                    // Report the successful created
                    response.status(201).send(); // 201 Created
                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: createEvent | Step: Event.create() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Group.create()

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: createGroup
     * Description: Creates the requested group.
     ******************************************************/
    createGroup: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var name;
            if (request.body.name) name = request.body.name.trim();

            // Create a new group
            var Group = mongoose.model('Group');
            var newGroup = new Group();
            newGroup.name = name;
            newGroup.owner = uid;
            newGroup.members.push(uid);

            // Insert the new group
            Group.create(newGroup)
                .then((group) => {
                    // Report the successful created
                    response.status(201).send(); // 201 Created
                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: createGroup | Step: Group.create() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Group.create()

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: deleteEvent
     * Description: Deletes the requested event.
     ******************************************************/
    deleteEvent: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var eventId;
            if (request.params.eventId) eventId = request.params.eventId.trim();

            // Ensure the record to remove was included
            if (!eventId) {
                response.status(400).send(); // Bad Request
            }

            // Lookup the requested Event
            var Event = mongoose.model('Event');
            Event.findById(eventId)
                .then((event) => {
                    if (event) {

                        // Only the event owner can remove the event
                        if (event.owner == uid) {

                            // Remove the Events
                            var Event = mongoose.model('Event');
                            Event.findOneAndRemove({ '_id': eventId })
                                .then(() => {
                                    response.status(200).send(event); // 200 Ok
                                })
                                .catch((error) => {
                                    log.error(`"In: ${__filename} | On: deleteEvent | Step: Event.findOneAndRemove() | Error: ${error}`);
                                    response.status(500).send(); // 500 Internal Server Error
                                }); // Event.remove()

                        } else {
                            response.status(401).send(); // 401 Unauthorized
                        }

                    } else {
                        response.status(404).send(); // 404 Not Found
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: deleteEvent | Step: Event.findById() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Group.findById()

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: deleteGroup
     * Description: Deletes the requested group.
     ******************************************************/
    deleteGroup: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var groupId;
            if (request.params.groupId) groupId = request.params.groupId.trim();

            // Ensure the record to remove was included
            if (!groupId) {
                response.status(400).send(); // Bad Request
            }

            // Lookup the requested group
            var Group = mongoose.model('Group');
            Group.findById(groupId)
                .then((group) => {
                    if (group) {

                        // Only the group owner can remove the group
                        if (group.owner == uid) {

                            // Remove the associated Events
                            var Event = mongoose.model('Event');
                            Event.remove({ 'group._id': group._id })
                                .then(() => {

                                    // Remove the group
                                    Group.findOneAndRemove({ '_id': groupId })
                                        .then(() => {
                                            response.status(200).send(group); // 200 Ok
                                        })
                                        .catch((error) => {
                                            log.error(`"In: ${__filename} | On: deleteGroup | Step: Group.findOneAndRemove() | Error: ${error}`);
                                            response.status(500).send(); // 500 Internal Server Error
                                        }); // Group.findOneAndRemove()

                                })
                                .catch((error) => {
                                    log.error(`"In: ${__filename} | On: deleteGroup | Step: Event.remove() | Error: ${error}`);
                                    response.status(500).send(); // 500 Internal Server Error
                                }); // Event.remove()

                        } else {
                            response.status(401).send(); // 401 Unauthorized
                        }

                    } else {
                        response.status(404).send(); // 404 Not Found
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: deleteGroup | Step: Group.findById() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Group.findById()

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: getGroupEvents
     * Description: Returns the requested events associated
     * with the requested group.
     ******************************************************/
    getGroupEvents: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            /* To-Do: */

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: getUser
     * Description: Returns the requested user.
     ******************************************************/
    getUser: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var userId;
            if (request.params.userId) userId = request.params.userId.trim();

            // Get the current user instead
            if (userId == 'current') userId = uid;

            /* To-Do: 
             * Apply security so that getUser requets only return results
             * for users of groups where the requesting user is also a member. 
             * */

            // Lookup the requested user
            var User = mongoose.model('User');
            User.findById(userId)
                .then((user) => {

                    if (user) {
                        // Convert doc to an object litteral
                        user = user.toObject();
                        // Remove protected fields
                        delete user.password;
                        // Send the requested user
                        response.status(200).send(user); // 200 Ok
                    } else {
                        response.status(404).send(); // 404 Not Found
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: getUser | Step: User.findById() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // User.findById

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: getUserEvents
     * Description: Returns the requested events associated
     * with the requested user.
     ******************************************************/
    getUserEvents: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var userId;
            if (request.params.userId) userId = request.params.userId.trim();

            // Users can only request their own events
            if (userId != uid) {
                response.status(401).send(); // 401 Unauthorized
            }

            // Lookup the requested event
            var Event = mongoose.model('Event');
            Event.find({
                    $or: [
                        { 'owner': mongoose.Types.ObjectId(userId) },
                        { 'whoChecksIn': mongoose.Types.ObjectId(userId) },
                        { 'whoIsNotified': mongoose.Types.ObjectId(userId) }
                    ]
                })
                .populate('group')
                .populate('owner')
                .populate('whoChecksIn')
                .populate('whoIsNotified')
                .exec()
                .then((events) => {

                    // Update the results before sending them
                    for (var i = 0, len = events.length; i < len; i++) {
                        // Convert doc to an object litteral
                        events[i] = events[i].toObject();

                        // Sort whoChecksIn by Name
                        events[i].whoChecksIn = events[i].whoChecksIn.sort(objHelper.dynamicSort('name'));

                        // Sort whoChecksIn by Name
                        events[i].whoIsNotified = events[i].whoIsNotified.sort(objHelper.dynamicSort('name'));
                    }

                    // Send the results
                    response.status(200).send(events); // 200 Ok
                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: getUserEvents | Step: Event.find() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Group.find()

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: getUserGroups
     * Description: Returns the requested groups associated
     * with the requested user.
     ******************************************************/
    getUserGroups: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var userId;
            if (request.params.userId) userId = request.params.userId.trim();

            // Users can only request their own groups
            if (userId != uid) {
                response.status(401).send(); // 401 Unauthorized
            }

            // Lookup the requested group
            var Group = mongoose.model('Group');
            Group.find({
                    $or: [
                        { 'owner': mongoose.Types.ObjectId(userId) },
                        { 'members': mongoose.Types.ObjectId(userId) }
                    ]
                })
                .populate('owner')
                .populate('members')
                .exec()
                .then((groups) => {

                    // Update the results before sending them
                    for (var i = 0, len = groups.length; i < len; i++) {
                        // Convert doc to an object litteral
                        groups[i] = groups[i].toObject();

                        // Convert Owner object to Owner.Name String
                        groups[i].owner = groups[i].owner.name;

                        // First sort the members
                        groups[i].members = groups[i].members.sort(objHelper.dynamicSort('name'));
                        // Convert array of Members objects to Member.Name String
                        var members = '';
                        for (var i2 = 0, len2 = groups[i].members.length; i2 < len2; i2++) {
                            members += groups[i].members[i2].name + ', ';
                        }
                        // Trim the trailing ', ' from the string
                        if (members.length > 2) {
                            members = members.slice(0, (members.length - 2));
                        }
                        groups[i].members = members;

                        // First sort the invited
                        groups[i].invited = groups[i].invited.sort();
                        // Convert array of Invited to Invited String
                        var invited = '';
                        for (var i3 = 0, len3 = groups[i].invited.length; i3 < len3; i3++) {
                            invited += groups[i].invited[i3] + ', ';
                        }
                        // Trim the trailing ', ' from the string
                        if (invited.length > 2) {
                            invited = invited.slice(0, (invited.length - 2));
                        }
                        groups[i].invited = invited;

                    }

                    // Send the results
                    response.status(200).send(groups); // 200 Ok
                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: getUserGroups | Step: Group.find() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Group.find()

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /*********************************************************
     * Function: inviteToGroup
     * Description: Invites requested email to join the group.
     *********************************************************/
    inviteToGroup: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            log.info('Called inviteToGroup')

            // Get the request values
            var groupId, email;
            if (request.params.groupId) groupId = request.params.groupId.trim();
            if (request.params.email) email = request.params.email.trim();

            // Ensure the record to update was included
            if (!groupId || !email) {
                response.status(400).send(); // Bad Request
            }

            // Lookup the requested group
            var Group = mongoose.model('Group');
            Group.findById(groupId)
                .then((group) => {
                    if (group) {

                        // Only group Owners can invite people to the group
                        if (group.owner == uid) {

                            log.info(`security passed`)

                            /* To-Do:
                             * Send an email to the person requesting for them to join the group.
                             */

                            log.info(`group.invited: ${group.invited}`)
                            log.info(`email: ${email}`)

                            log.info(`group.invited.indexOf(email)` + group.invited.indexOf(email))

                            // Check if the email is already in the list of invited
                            if (group.invited.indexOf(email) > -1) {
                                // If it's already there, don't add a duplicate
                                response.status(200).send(); // 200 Ok
                            } else {
                                // If not, then add it
                                group.invited.push(email);
                                // Commit updates
                                group.save((error, user) => {
                                    if (error) {
                                        log.error(`"In: ${__filename} | On: updateGroup | Step: group.save() | Error: ${error}`);
                                        response.status(500).send(); // 500 Internal Server Error
                                    } else {
                                        response.status(200).send(); // 200 Ok
                                    }
                                });
                            }

                        } else {
                            response.status(401).send(); // 401 Unauthorized
                        }

                    } else {
                        response.status(404).send(); // 404 Not Found
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: updateGroup | Step: Group.findById() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Group.findById()

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: updateEvent
     * Description: Updates the requested event
     ******************************************************/
    updateEvent: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var enabled, eventId, frequency, name, notes, recurDays, recurMonths, recurMonthOnDays;
            var recurWeeks, recurWeekOnDays, startDate, whoChecksIn, whoIsNotified;
            if (request.body.enabled) enabled = (request.body.enabled === true);
            if (request.body.eventId) eventId = request.body.eventId.trim();
            if (request.body.frequency) frequency = request.body.frequency.trim();
            if (request.body.name) name = request.body.name.trim();
            if (request.body.notes) notes = request.body.notes.trim();
            if (request.body.recurDays) recurDays = request.body.recurDays;
            if (request.body.recurMonths) recurMonths = objHelper.arrayTrimStrings(request.body.recurMonths);
            if (request.body.recurMonthOnDays) recurMonthOnDays = objHelper.arrayTrimStrings(request.body.recurMonthOnDays);
            if (request.body.recurWeeks) recurWeeks = request.body.recurWeeks;
            if (request.body.recurWeekOnDays) recurWeekOnDays = objHelper.arrayTrimStrings(request.body.recurWeekOnDays);
            if (request.body.startDate) startDate = request.body.startDate;
            if (request.body.whoChecksIn) startDate = objHelper.arrayTrimStrings(request.body.whoChecksIn);
            if (request.body.whoIsNotified) startDate = objHelper.arrayTrimStrings(request.body.whoIsNotified);

            // Ensure the record to update was included
            if (!eventId) {
                response.status(400).send() // Bad Request
            }

            // Lookup the requested event
            var Event = mongoose.model('Event');
            Event.findById(eventId)
                .then((event) => {
                    if (event) {

                        // Only event Owners can update the event
                        if (event.owner == uid) {

                            // Update the fields
                            event.enabled = enabled;
                            if (frequency) { event.frequency = frequency; }
                            if (name) { event.name = name; }
                            if (notes) { event.notes = notes; }
                            if (recurDays) { event.recurDays = recurDays; }
                            if (recurMonths) { event.recurMonths = recurMonths; }
                            if (recurMonthOnDays) { event.recurMonthOnDays = recurMonthOnDays; }
                            if (recurWeeks) { event.recurWeeks = recurWeeks; }
                            if (recurWeekOnDays) { event.recurWeekOnDays = recurWeekOnDays; }
                            if (startDate) { event.startDate = startDate; }
                            if (whoChecksIn) { event.whoChecksIn = whoChecksIn; }
                            if (whoIsNotified) { event.whoIsNotified = whoIsNotified; }

                            // Commit updates
                            event.save()
                                .then(() => {
                                    response.status(200).send(); // 200 Ok
                                })
                                .catch((error) => {
                                    log.error(`"In: ${__filename} | On: updateEvent | Step: event.save() | Error: ${error}`);
                                    response.status(500).send(); // 500 Internal Server Error
                                }); // event.save()

                        } else {
                            response.status(401).send(); // 401 Unauthorized
                        }

                    } else {
                        response.status(404).send(); // 404 Not Found
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: updateEvent | Step: Event.findById() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Event.findById

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: updateGroup
     * Description: Updates the requested group
     ******************************************************/
    updateGroup: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var name, groupId;
            if (request.body.name) name = request.body.name.trim();
            if (request.params.groupId) groupId = request.params.groupId.trim();

            // Ensure the record to update was included
            if (!groupId) {
                response.status(400).send() // Bad Request
            }

            // Lookup the requested group
            var Group = mongoose.model('Group');
            Group.findById(groupId)
                .then((group) => {
                    if (group) {

                        // Only group Owners can update the group
                        if (group.owner == uid) {

                            // Update the fields
                            if (name) { group.name = name; }

                            // Commit updates
                            group.save()
                                .then(() => {
                                    response.status(200).send(); // 200 Ok
                                })
                                .catch((error) => {
                                    log.error(`"In: ${__filename} | On: updateGroup | Step: group.save() | Error: ${error}`);
                                    response.status(500).send(); // 500 Internal Server Error
                                }); // group.save()

                        } else {
                            response.status(401).send(); // 401 Unauthorized
                        }

                    } else {
                        response.status(404).send(); // 404 Not Found
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: updateGroup | Step: Group.findById() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Group.findById

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

    /******************************************************
     * Function: updateUser
     * Description: Updates the requested user
     ******************************************************/
    updateUser: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Get the request values
            var email, mobile, name, password, userId;
            if (request.body.email) email = request.body.email.trim();
            if (request.body.mobile) mobile = request.body.mobile.trim();
            if (request.body.name) name = request.body.name.trim();
            if (request.body.password) password = request.body.password.trim();
            if (request.params.userId) userId = request.params.userId.trim();

            // Ensure the record to update was included
            if (!userId) {
                response.status(400).send(); // Bad Request
            }

            // Lookup the requested user
            var User = mongoose.model('User');
            User.findById(userId)
                .then((user) => {
                    if (user) {

                        // A user can only update themselves
                        if (user._id == uid) {

                            // Update the fields
                            if (email) { user.email = email; }
                            if (mobile) { user.mobile = mobile; }
                            if (name) { user.name = name; }
                            if (password) { user.password = password; }

                            // Commit updates
                            user.save()
                                .then(() => {
                                    response.status(200).send(); // 200 Ok
                                })
                                .catch((error) => {
                                    log.error(`"In: ${__filename} | On: updateUser | Step: user.save() | Error: ${error}`);
                                    response.status(500).send(); // 500 Internal Server Error
                                }); // user.save()

                        } else {
                            response.status(401).send(); // 401 Unauthorized
                        }

                    } else {
                        response.status(404).send(); // 404 Not Found
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: updateUser | Step: User.findById() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // User.findById

        } else {
            response.status(401).send(); // 401 Unauthorized
        }
    },

};