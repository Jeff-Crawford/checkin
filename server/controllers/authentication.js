// Load dependant modules
var bcrypt = require('bcryptjs');
var fs = require('fs');
var mongoose = require('mongoose');
var path = require('path');

// Settings & Configuration
const ENV = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const CONFIG = require('../config/config.js')[ENV];

// Initialize modules
var log = require('../config/logger.js');
var mailgun = require('../config/mailgun.js');

module.exports = {

    /******************************************************
     * Function: activation
     * Description: Accepts an activation link and converts
     * to a user.
     ******************************************************/
    activation: (request, response) => {
        var uuId = request.params.uuId;
        if (uuId) uuId = uuId.toUpperCase().trim();
        if (uuId) {

            // Check registration for the provided code
            var Registration = mongoose.model('Registration');
            Registration.findOne({ "uuId": uuId })
                .then((registration) => {
                    if (registration) {

                        // Create a new user
                        var User = mongoose.model('User');
                        var newUser = new User();
                        newUser.email = registration.email;
                        newUser.password = registration.password;
                        newUser.skipHashing = true; // Because the password is already hashed
                        User.create(newUser)
                            .then((user) => {

                                // After creating a user, delete the registration
                                registration.remove()
                                    .then(() => {
                                        response.redirect(`${CONFIG.hostUrl}/#!/login`);
                                    })
                                    .catch((error) => {
                                        log.error(`"In: ${__filename} | On: activation | Step: registration.remove() | Error: ${error}`);
                                        response.status(500).send(); // 500 Internal Server Error
                                    }); // registration.remove()

                            })
                            .catch((error) => {
                                log.error(`"In: ${__filename} | On: activation | Step: User.create() | Error: ${error}`);
                                response.status(500).send(); // 500 Internal Server Error
                            }); // User.create()

                    } else {
                        response.status(404).send(); // 404 Not Found
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: activation | Step: Registration.findOne() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // Registration.findOne()

        } else {
            response.status(400).send(); // 400 Bad Request
        }
    }, // activation

    /******************************************************
     * Function: authenticated
     * Description: Check to see if the request has is 
     * authenticated.
     ******************************************************/
    authenticated: (request, response) => {
        var uid = request.session.uid;
        if (uid) {

            // Lookup uid
            var User = mongoose.model('User');
            User.findOne({ "_id": uid })
                .then((user) => {
                    if (user) {
                        response.status(200).send(); // 200 Ok
                    } else {
                        request.session.uid = null;
                        response.status(401).send(); // 401 Unauthorized
                    }
                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: authenticated | Step: User.findOne() | Error: ${error}`);
                    request.session.uid = null;
                    response.status(401).send(); // 401 Unauthorized
                }); // User.findOne()

        } else {
            request.session.uid = null;
            response.status(401).send(); // 401 Unauthorized
        }
    }, //authenticated

    /******************************************************
     * Function: login
     * Description: Accepts login info, verifies it and then
     * authenicates it.
     ******************************************************/
    login: (request, response) => {
        var email, password
        if (request.body.email) email = request.body.email.toLowerCase().trim();
        if (request.body.password) password = request.body.password.trim();
        if (email && password) {

            // Looks up the user
            var User = mongoose.model('User');
            User.findOne({ "email": email })
                .then((user) => {

                    if (user) {
                        // Compare the provided password with the hashed user.password
                        bcrypt.compare(password, user.password, (error, matched) => {
                            if (error) {
                                log.error(`"In: ${__filename} | On: login | Step: bcrypt.compare() | Error: ${error}`);
                                equest.session.uid = null;
                                response.status(500).send(); // 500 Internal Server Error
                            }
                            if (matched) {
                                request.session.uid = user._id;
                                response.status(200).send(); // 200 Ok
                            } else {
                                request.session.uid = null;
                                response.status(401).send(); // 401 Unauthorized
                            }
                        })

                    } else {
                        request.session.uid = null;
                        response.status(401).send(); // 401 Unauthorized
                    }

                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: login | Step: User.findOne() | Error: ${error}`);
                    request.session.uid = null;
                    response.status(500).send(); // 500 Internal Server Error
                }); // User.findOne()

        } else {
            request.session.uid = null;
            response.status(400).send(); // 400 Bad Request
        }
    }, // login

    /******************************************************
     * Function: logout
     * Description: Logs out the current user
     ******************************************************/
    logout: (request, response) => {
        request.session.uid = null;
        response.status(200).send(); // 200 Ok
    }, // logout

    /******************************************************
     * Function: registration
     * Description: Accepts a registration and emails them
     * an activation link.
     ******************************************************/
    registration: (request, response) => {
        var email, password
        if (request.body.email) email = request.body.email.toLowerCase().trim();
        if (request.body.password) password = request.body.password.trim();
        if (email && password) {

            // Ensure the provided email is not already a user
            var User = mongoose.model('User');
            User.findOne({ "email": email })
                .then((user) => {

                    if (user) {
                        response.status(409).send(); // 409 Conflict
                    } else {

                        // Ensure the provided email does not have a registration
                        var Registration = mongoose.model('Registration');
                        Registration.findOneAndRemove({ "email": email })
                            .then(() => {

                                // Create a new registration
                                var newRegistration = new Registration();
                                newRegistration.email = email;
                                newRegistration.password = password;
                                Registration.create(newRegistration)
                                    .then((registration) => {

                                        // Get the activate email template
                                        fs.readFile(path.join(__dirname, '../templates/activate.email.html'), "utf-8", (error, data) => {
                                            if (error) {
                                                log.error(`"In: ${__filename} | On: registration | Step: fs.readFile() | Error: ${error}`);
                                                response.status(500).send(); // 500 Internal Server Error
                                            } else {

                                                // Build the activation link
                                                var url = `${CONFIG.hostUrl}/api/activation/${registration.uuId}`;
                                                var htmlData = data.replace('<a type="button" href=""', `<a type="button" href="${url}"`);

                                                // Build and send the email
                                                var emailData = {
                                                    "from": 'checkIn <noreply@checkin.jeffcrawford.us>',
                                                    "to": registration.email,
                                                    "subject": 'Activate checkIn Account',
                                                    "html": htmlData
                                                };
                                                mailgun.messages().send(emailData, (error, body) => {
                                                    if (error) {
                                                        log.error(`"In: ${__filename} | On: registration | Step: mailgun.messages() | Error: ${error}`);
                                                        response.status(500).send(); // 500 Internal Server Error
                                                    } else {
                                                        response.status(201).send(); // 201 Created
                                                    }
                                                }); //mailgun.messages()
                                            }
                                        }); //fs.readFile()
                                    })
                                    .catch((error) => {
                                        log.error(`"In: ${__filename} | On: registration | Step: Registration.create() | Error: ${error}`);
                                        response.status(500).send(); // 500 Internal Server Error
                                    }); //Registration.create()

                            })
                            .catch((error) => {
                                log.error(`"In: ${__filename} | On: registration | Step: Registration.findOneAndRemove() | Error: ${error}`);
                                response.status(500).send(); // 500 Internal Server Error
                            }); //Registration.findOneAndRemove()
                    }
                })
                .catch((error) => {
                    log.error(`"In: ${__filename} | On: registration | Step: Registration.findOneAndRemove() | Error: ${error}`);
                    response.status(500).send(); // 500 Internal Server Error
                }); // User.findOne()
        } else {
            response.status(400).send(); // 400 Bad Request
        }
    }, // registration

};