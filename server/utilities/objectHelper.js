// Gets the index of an array by property value
module.exports.arrayObjectIndexOf = function(arr, prop, val) {
    for (var i = 0, len = arr.length; i < len; i++) {
        if (arr[i][prop] === val) {
            return i;
        }
    }
    return -1;
}

// Trims each string in the array
module.exports.arrayTrimStrings = function(arr) {
    return arr.map(function(str) {
        if (str && (typeof str == 'string')) {
            return str.trim();
        } else {
            return str;
        }
    });
}

// Sort call back that sorts an array by property
module.exports.dynamicSort = function(prop) {
    var sortOrder = 1;
    if (prop[0] === "-") {
        sortOrder = -1;
        prop = prop.substr(1);
    }
    return function(a, b) {
        var result = (a[prop] < b[prop]) ? -1 : (a[prop] > b[prop]) ? 1 : 0;
        return result * sortOrder;
    }
}