// Load dependant modules
var auth = require('../controllers/authentication.js');
var entity = require('../controllers/entities.js');

module.exports = (app) => {
    // Public Rotues //
    app.get('/', (request, response) => {
        response.sendFile('index.html', { root: './public' });
    });

    // Server Routes //

    /* Authentication */
    app.get('/api/authenticated', auth.authenticated);
    app.get('/api/activation/:uuId', auth.activation);
    app.post('/api/login', auth.login);
    app.post('/api/logout', auth.logout);
    app.post('/api/registration', auth.registration);

    /* Events */
    app.post('/api/events/', entity.createEvent);
    app.delete('/api/events/:eventId', entity.deleteEvent);
    app.put('/api/events/:eventId', entity.updateEvent);

    /* Groups */
    app.post('/api/groups/', entity.createGroup);
    app.delete('/api/groups/:groupId', entity.deleteGroup);
    app.put('/api/groups/:groupId', entity.updateGroup);
    app.put('/api/groups/:groupId/invite/:email', entity.inviteToGroup);

    /* Users */
    app.get('/api/users/:userId', entity.getUser);
    app.get('/api/users/:userId/events', entity.getUserEvents);
    app.get('/api/users/:userId/groups', entity.getUserGroups);
    app.put('/api/users/:userId', entity.updateUser);
};