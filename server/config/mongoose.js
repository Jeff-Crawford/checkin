// Load the Modules
var mongoose = require('mongoose');
var log = require('./logger.js');

// To get Mongoose from complaining about it's promise model
mongoose.Promise = Promise;

// Require Model(s)
require('../models/event.js');
require('../models/group.js');
require('../models/registration.js');
require('../models/user.js');

// Connect to MongoDB
module.exports = function(db) {

    // On MongoDB Connection
    mongoose.connection.on('connected', () => {
        log.info(`Mongoose connected to ${db}`);
    });

    // On MongoDB Error
    mongoose.connection.on('error', (error) => {
        log.error(`Mongoose connection error: ${error}`);
    });

    // On MongoDB Disconnect
    mongoose.connection.on('disconnected', () => {
        log.info(`Mongoose disconnected from ${db}`);
    });

    // Make the DB Connection
    mongoose.connect(db)
        .catch(error => {
            log.error(`Mongoose connection error: ${error}`);
        });
};