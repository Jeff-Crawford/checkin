// Settings & Configuration
const ENV = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const CONFIG = require('./config.js')[ENV];

// Start an instance of bunyan and export it for modular use.
var logger = require('bunyan')
    .createLogger({
        name: CONFIG.app
    });
module.exports = logger;