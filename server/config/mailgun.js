// Settings & Configuration
const ENV = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const CONFIG = require('./config.js')[ENV];

// Start an instance of mailgun and export it for modular use.
var mailgun = require('mailgun-js')({
    apiKey: CONFIG.mailgunAPIKey,
    domain: CONFIG.mailgunDomain
});
module.exports = mailgun;