// Load dependant modules
var mongoose = require('mongoose');

// Create the schema
var groupSchema = new mongoose.Schema({
    createdOn: { type: Date },
    name: { type: String },
    owner: { type: mongoose.Schema.ObjectId, ref: 'User' },
    members: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
    invited: [{ type: String }],
});

// Create and Save events
groupSchema.pre('save', function(next) {

    // Update the CreatedOn Date
    if (!this.createdOn) {
        this.createdOn = Date.now();
    }

    return next();
});

// Add the schema model
mongoose.model('Group', groupSchema, 'groups');