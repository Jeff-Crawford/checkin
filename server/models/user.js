// Load dependant modules
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');

// Create the schema
var userSchema = new mongoose.Schema({
    avatar: { type: String },
    createdOn: { type: Date },
    email: { type: String, required: true, unquie: true },
    isEmailVerified: { type: Boolean },
    isMobileVerified: { type: Boolean },
    name: { type: String },
    password: { type: String, required: true },
    mobile: { type: String },
    skipHashing: { type: Boolean }
});

// Create and Save events
userSchema.pre('save', function(next) {

    // Update the CreatedOn Date
    if (!this.createdOn) {
        this.createdOn = Date.now();
    }

    // Ensure email is lower case for lookups
    if (this.email) {
        this.email = this.email.toLowerCase();
    }

    // Registration hashes as well, so don't do it again
    if (this.skipHashing === true) {
        this.skipHashing = undefined;
        return next();
    }

    // Only hash the password if it is new or modified
    if (!this.isModified('password')) {
        return next();
    }

    // Salt and Hash updated passwords
    bcrypt.genSalt(10, (saltErr, salt) => {
        if (saltErr) {
            return next(saltErr);
        }
        bcrypt.hash(this.password, salt, (hashErr, hashedPassword) => {
            if (hashErr) {
                return next(hashErr);
            }
            this.password = hashedPassword;
            return next();
        });
    });
});

// Add the schema model
mongoose.model('User', userSchema, 'users');