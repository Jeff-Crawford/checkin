// Load dependant modules
var mongoose = require('mongoose');

// Create the schema
var eventSchema = new mongoose.Schema({
    createdOn: { type: Date },
    enabled: { type: Boolean },
    frequency: { type: String },
    group: { type: mongoose.Schema.ObjectId, ref: 'Group' },
    name: { type: String },
    notes: { type: String },
    owner: { type: mongoose.Schema.ObjectId, ref: 'User' },
    recurDays: { type: Number },
    recurMonths: [{ type: String }],
    recurMonthOnDays: [{ type: String }],
    recurWeeks: { type: Number },
    recurWeekOnDays: [{ type: String }],
    startDate: { type: Date },
    whoChecksIn: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
    whoIsNotified: [{ type: mongoose.Schema.ObjectId, ref: 'User' }]
});

// Create and Save events
eventSchema.pre('save', function(next) {

    // Update the CreatedOn Date
    if (!this.createdOn) {
        this.createdOn = Date.now();
    }

    return next();
});

// Add the schema model
mongoose.model('Event', eventSchema, 'events');