// Load dependant modules
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');
var uuidV4 = require('uuid/v4');

// Create the schema
var userRegistration = new mongoose.Schema({
    createdOn: { type: Date },
    email: { type: String, required: true, unquie: true },
    password: { type: String, required: true },
    uuId: { type: String }
});

// Create and Save events
userRegistration.pre('save', function(next) {

    // Update the CreatedOn Date
    if (!this.createdOn) {
        this.createdOn = Date.now();
    }

    // Ensure email is lower case for lookups
    if (this.email) {
        this.email = this.email.toLowerCase();
    }

    // Keep a long code for the URL validation
    if (!this.uuId) {
        // Generate a new RFC4122 UUID version 4
        this.uuId = uuidV4().toUpperCase();
    }

    // Only hash the password if it is new or modified
    if (!this.isModified('password')) {
        return next();
    }

    // Salt and Hash updated passwords
    bcrypt.genSalt(10, (saltErr, salt) => {
        if (saltErr) {
            return next(saltErr);
        }
        bcrypt.hash(this.password, salt, (hashErr, hashedPassword) => {
            if (hashErr) {
                return next(hashErr);
            }
            this.password = hashedPassword;
            return next();
        });
    });

});

// Add the schema model
mongoose.model('Registration', userRegistration, 'registrations');