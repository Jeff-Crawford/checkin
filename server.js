// Load dependant modules
var bodyParser = require('body-parser');
var compression = require('compression');
var express = require('express');
var mongoose = require('mongoose');
var morgan = require('morgan')
var routes = require('./server/config/routes.js');
var sessions = require("client-sessions");

// Settings & Configuration
const ENV = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const CONFIG = require('./server/config/config.js')[ENV];
const PORT = process.env.PORT || CONFIG.port;

// Initialize modules
var app = express();
var log = require('./server/config/logger.js');
require('./server/config/mongoose.js')(CONFIG.db);

// Log which envirorment variable is being used
log.info(`ENV: ${ENV}`);

// Apply middleware
app.use(
    bodyParser.json(),
    bodyParser.urlencoded({ "extended": true }),
    compression(),
    express.static('./public'),
    morgan('dev'),
    sessions({
        cookieName: CONFIG.app, // cookie name dictates the key name added to the request object
        secret: CONFIG.sessionKey, // should be a large unguessable string
        duration: 7 * 24 * 60 * 60 * 1000, // how long the session will stay valid in ms
        requestKey: 'session', // request.session
        cookie: {
            ephemeral: false, // when true, cookie expires when the browser closes
            httpOnly: true, // when true, cookie is not accessible from javascript
            secure: false // when true, cookie will only be sent over SSL
        }
    })
);
//app.use(checkIfAuthenticated, express.static('./protected'))
// Pass the requests through to the routes
routes(app);

// Startup Express
var server = app.listen(PORT, (error) => {
    if (error) {
        log.error(`Express startup error: ${error}`)
    } else {
        log.info(`Express Server is now listening on port: ${PORT}`);
    }
});

// On Node exit
process.on('exit', (code) => {
    log.info(`Node exited with code: ${code}`);
    mongoose.disconnect();
});


// On Node uncaught exception
process.on('uncaughtException', (error) => {
    if (error) {
        log.error(`Node unhandled error: ${error}`);
    }
});

// On Node kill signals
process.on('SIGINT', shutdownServer);
process.on('SIGTERM', shutdownServer);

// Shutdown and exit when kill signals are received
function shutdownServer() {
    // Log the shutdown
    log.info('Node received a kill signal and is now shutting down.');

    // Disconnect from MongoDB
    mongoose.disconnect();

    // Have Express stop accepting requests, finish up, & and exit Node
    server.close(() => {
        process.exit();
    });

    // Force the Node to exit after 10 seconds
    setTimeout(() => {
        process.exit();
    }, 10 * 1000);
};