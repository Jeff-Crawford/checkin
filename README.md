# [checkIn](checkin.jeffcrawford.us)

Check in with family and friends made easy.

## Creator

checkIn was created by and is maintained by **[Jeff Crawford](me@jeffcrawford.us)**

* https://github.com/Jeff-Crawford/checkin

## Copyright and License

UNLICENSED
